#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

//#include "main.h"


int main() {
	uint32_t array1[3][3]; //2D array
	uint32_t array2[3][3][3]; //3D array
	printf("2-D Array\n");
	for(int a = 0; a < 3; a++) {
		for(int b = 0; b < 3; b++) {
			printf("Address of [%d][%d] = %p\n", a, b, &array1[a][b]);
		}
	}
	printf("3-D Array\n");
	for(int a = 0; a < 3; a++) {
		for(int b = 0; b < 3; b++) {
			for(int c = 0; c < 3; c++) {
				printf("Address of [%d][%d][%d] = %p\n", a, b, c, &array2[a][b][c]);
			}
		}
	}
}

