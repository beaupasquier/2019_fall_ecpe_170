#ifndef CFOUR_H
#define CFOUR_H

void print_board();
void init_board();
unsigned int get_random();
void gameloop();
int update_board(int move, char plr);
int check_win(char *addr);

#endif // CFOUR_H
