#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "connect_five.h"

char board[49] = ".................................................";
unsigned int m_z,m_w;
int main(void) {
	printf("Welcome to Connect Five \nEnter two positive numbers to initialize the random number generator.\nRandom1:");
	scanf("%d",&m_z);
	printf("Random2:");
	scanf("%d",&m_w);
	gameloop();
	printf("Thank you for playing!\n");
	return 0;
}

void print_board() {
	char* addr = board+48;
	printf("1 2 3 4 5 6 7\n");
	printf("-------------\n");
	for (int r = 0; r < 7; r++) {
		printf("%c",*(addr));
		printf("%s", " ");
		printf("%c",*(addr-1));
		printf("%s", " ");
		printf("%c",*(addr-2));
		printf("%s", " ");
		printf("%c",*(addr-3));
		printf("%s", " ");
		printf("%c",*(addr-4));
		printf("%s", " ");
		printf("%c",*(addr-5));
		printf("%s", " ");
		printf("%c",*(addr-6));
		printf("%s", " ");
		printf("%c",'\n');
		addr -= 7;
	}
	printf("-------------\n");
}

/*From lab 11 but now returns a number between 0 and 6*/
unsigned int get_random()
{
  m_z = 36969 * (m_z & 65535) + (m_z >> 16);
  m_w = 18000 * (m_w & 65535) + (m_w >> 16);
  return (((m_z << 16) + m_w) % 7)+1;/* 32-bit result */
}


void gameloop() {
	unsigned int drop = 0;
	while(1) {
		print_board();
		printf("What column would you like to drop in? (0 to quit) ");
		scanf("%d",&drop);
		if (drop < 0 || drop > 7) {
			printf("That's not a valid column\n");
			continue;
		}
		if (drop == 0) {
			printf("Goodbye\n");
			return;
		}
		int piece_drop = update_board(drop,'H');
		if (piece_drop == -1) {
			printf("That's not a valid column\n");
			continue;
		}
		//check each turn for 5 in a row
		if (check_win((char*)(board + piece_drop)) == 1) {
			print_board();
			printf("Congratulations! You win!\n");
			return;
		}
		//AI move
		do {
			drop = get_random();
			piece_drop = update_board(drop,'C');
		} while (piece_drop == -1);
		printf("Computer selected column %d\n",drop);
		if (check_win((char*)(board + piece_drop)) == 1) {
			print_board();
			printf("You lose! AI winner! \n");
			return;
		}
	}
}

/*
	returns the index that it dropped the piece at, or -1 if it didn't
*/
int update_board(int move, char plr) {
	char* addr = board + (7-move);
	int i = 0;
	do {
		if(*addr == '.') {
			*addr = plr;
			return addr - board;
		}
		addr += 7;
		i++;
	} while (i < 7);
	return -1;
}


/*
*/
int check_win(char *addr) {
	int row = (addr - board) / 7;
	int col = (addr - board) % 7;
	

/* Check for a vertical victory (only top piece for matching pieces below)*/
	if (row >= 4) {
		if ( *(addr-7) == *addr && *(addr-14) == *addr && *(addr-21) == *addr && *(addr-28) == *addr) {
			return 1;
		}
	}

/*Horizontal Victory (check total of 5 pieces to left or right) */
	int left = 0;
	int right = 0;
	int maxL = 7-col;
	int maxR = col;
	for (int i=1; i<maxL; i++) {
		if (*(addr+i) != *addr) {break;}
		left++;
	}
	for (int i=1; i<maxR; i++) {
		if (*(addr-i) != *addr) {break;}
		right++;
	}
	if(right + left > 3) {
		return 1;
	}

/* Diagonal win check (horizontal check just w vertical component) */

	/*up left and down right*/
	left = 0;
	right = 0; 
	maxL = (7-row) < (7-col) ? (7-row) : (7-col);
	maxR = (row+1) < (col+1) ? (row+1) : (col+1);
	for (int i=1; i<maxL; i++) {
		if (*(addr+8*i) != *addr) {break;}
		left++;
	}
	for (int i=1; i<maxR; i++) {
		if (*(addr-8*i) != *addr) {break;}
		right++;
	}
	if(right + left > 3) {
		return 1;
	}

	/* up right and down left */
	left = 0;
	right = 0;
	maxL = (row+1) < (7-col) ? (row+1) : (7-col);
	maxR = (7-row) < (col+1) ? (7-row) : (col+1);
	for (int i=1; i<maxL; i++) {
		if (*(addr-6*i) != *addr) {break;}
		left++;
	}
	for (int i=1; i<maxR; i++) {
		if (*(addr+6*i) != *addr) {break;}
		right++;
	}
	if(left + right > 3) {
		return 1;
	}
	return 0;
}



























