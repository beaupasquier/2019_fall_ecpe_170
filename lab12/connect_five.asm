.globl main 
.text 		

main:
	# s0: board
	# s1: m_z
	# s2: m_w
	la 		$s0,board			# load the board
	la 		$a0,rand1			# load the intro message
	li 		$v0,4				# print string
	syscall						# call print string
	li		$v0,5				# read int
	syscall						# take int as input
	move 	$s1,$v0				# load into m_z
	la		$a0,rand2			# load the "random2:"
	li		$v0,4				# print string
	syscall						# call print string
	li		$v0,5				# read int
	syscall						# take int as input
	move	$s2,$v0				# load into m_w
	jal 	gameloop			# start game/gameloop
	j		exit				# close program
	
	
	
gameloop:
	# s3: drop
	addi 	$sp,$sp,-4 			# adjust stack pointer
	sw 		$ra, 0($sp) 		# push ra
gamewhile:
	# human's turn - print and input
	jal 	print_board			# print board
	la 		$a0,turn			# load the intro message for printing
	li 		$v0,4				# print string
	syscall						# call print string
	li		$v0,5				# read int
	syscall						# take int as input
	
	#check input
	move	$s3,$v0				# move the input into drop (s3)
	blt		$s3,0,bad_input		# check drop < 0
	bgt		$s3,7,bad_input		# check drop > 7
	beq		$s3,0,quit_game		# check drop == 0
	move	$a0,$s3				# prep drop as arg1
	li		$a1,72				# prep 'H' as arg2 (for visual)
	
	# Human turn
	jal		update_board		# call update_board
	beq		$v0,-1,bad_input	# if it didn't update, jump to bad input
	move	$a0,$v0 			# prep the space that you moved in for check_win()
	add		$a0,$a0,$s0			# board + piece_drop
	jal		check_win			# call check_win
	beq		$v0,1,win_game		# end the game if win
	
	#AI turn
AIloop:
	jal		get_random			# call get_random
	move	$s3,$v0				# drop = get_random()
	move	$a0,$s3				# prep for update_board call
	li		$a1,67				# arg2 = 'C'
	jal		update_board		# update_board(drop,'C')
	move	$s4,$v0				# store result in s4
	beq		$s4,-1,AIloop		# loop back if it was a bad choice
	move	$s3,$v0				# prep the space that you moved in for check_win()
	add		$a0,$s3,$s0			# board + piece_drop
	jal		check_win			# call check_win
	beq		$v0,1,lose_game		# if you lose, jump
	
	# check and print AI
	la		$a0,comp			# load computer text
	li		$v0,4				# 4=print_string
	syscall						# print it
	j		gamewhile			# loop back
bad_input:	
	la		$a0,invalid			# load bad input message
	li		$v0,4				# 4=print_string
	syscall						# print it
	j		gamewhile			# back to the game loop (basically a continue)
quit_game:
	la		$a0,quit			# load quit message
	li		$v0,4				# 4=print_string
	syscall						# print it
	lw		$ra,0($sp) 			# pop ra
	addi 	$sp,$sp,4 			# adjust stack pointer
	jr		$ra					# return
win_game:
	jal		print_board			# print the board
	la		$a0,win				# load win message
	li		$v0,4				# 4=print_string
	syscall						# print it
	lw		$ra,0($sp) 			# pop ra
	addi 	$sp,$sp,4 			# adjust stack pointer
	jr		$ra					# return
lose_game:
	jal		print_board			# print the board
	la		$a0,lose			# load win message
	li		$v0,4				# 4=print_string
	syscall						# print it
	lw		$ra,0($sp) 			# pop ra
	addi 	$sp,$sp,4 			# adjust stack pointer
	jr		$ra					# return
	
	
	
update_board:
	# a0: player's (or AI) move (and then becomes address)
	# a1: player
	
	# set up for loop
	sub		$a0,$s0,$a0			# addr = board - move
	addi	$a0,$a0,7			# add 7 to make addr=board+7-move
	li		$t0,0				# i=0
updateloop:
	lb		$t1,($a0)			# load for comparison
	beq		$t1,46,update_makemove	# check if the space is empty
	addi	$a0,$a0,7			# addr += 7
	addi	$t0,$t0,1			# i++
	blt 	$t0,7,updateloop	# check escape condition	
	li		$v0,-1				# false
	jr		$ra;				# return
update_makemove:
	sb		$a1,($a0)			# place the piece		
	sub		$v0,$a0,$s0			# v0=addr-board
	jr		$ra					# return 



check_win:
	# a0: addr
	# t0: i
	# s6: row
	# s5: col
	# t1: left
	# t2: right
	# t3: maxL
	# t4: maxR
	# s7: *addr
	
	# setup
	sub		$s6,$a0,$s0			# row = addr-board
	div		$s6,$s6,7			# row/7
	sub		$s5,$a0,$s0			# col = addr-board
	rem		$s5,$s5,7			# col%7

	
	# check vertical (only top piece and below)
	
	lb		$s7,($a0)				# *addr
	blt		$s6,4,cw_false1		# if row >= 4
	lb		$t5,-7($a0)
	bne		$s7,$t5,cw_false1		# *(addr-7)==*addr
	lb		$t5,-14($a0)
	bne		$s7,$t5,cw_false1		# *(addr-14)==*addr
	lb		$t5,-21($a0)
	bne		$s7,$t5,cw_false1		# *(addr-21)==*addr
	lb		$t5,-28($a0)
	bne		$s7,$t5,cw_false1		# *(addr-28)==*addr	
	j		cw_truereturn				# return true



cw_false1:
	
	
	# check horizontal
	
	
	li		$t1,0				# left = 0
	li		$t2,0				# right = 0;
	li		$t5,7
	sub		$t3,$t5,$s5			# maxL = 7-col
	move	$t4,$s5				# maxR = col
	
	# count left horizontal
	li		$t0,1				# i=1


cw_forl1:
	bge		$t0,$t3,cw_forl1_e	# check escape condition
	add		$t5,$a0,$t0			# addr+i
	lb		$t9,($t5)			# char at
	bne		$s7,$t9,cw_forl1_e	# break if *(addr+i)!=*addr
	addi	$t1,$t1,1			# left++
	addi	$t0,$t0,1			# i++
	j		cw_forl1			# loop back

cw_forl1_e:

	# count right horizontal
	li		$t0,1				# i=1

cw_forr1:
	bge		$t0,$t4,cw_forr1_e	# check escape condition
	sub		$t5,$a0,$t0			# addr+i
	lb		$t9,($t5)			# char at
	bne		$s7,$t9,cw_forr1_e	# break if *(addr+i)!=*addr
	addi	$t2,$t2,1			# right++
	addi	$t0,$t0,1			# i++
	j		cw_forr1			# loop back

cw_forr1_e:
	
	# sum horizontal points
	add		$t5,$t1,$t2			# t5=t1+t2
	bgt		$t5,3,cw_truereturn	#return true if > 3

	
	# diagonal check - down right and up left
	
	li		$t1,0				# left = 0
	li		$t2,0				# right = 0;
	
	# maxL = (7-row) < (7-col) ? (7-row) : (7-col);
	li		$t9,7				# t9=7
	sub		$t6,$t9,$s6			# t6=7-row
	sub		$t5,$t9,$s5			# t5=7-col
	blt		$t6,$t5,cw_ml_t1	# (7-row) < (7-col)
	move 	$t3,$t5				# maxL = 7-col
	j		cw_ml_e1			# escape

cw_ml_t1:
	move	$t3,$t6				# maxL = 7-row

cw_ml_e1:

	# maxR = (row+1) < (col+1) ? (row+1) : (col+1);
	addi	$t6,$s6,1			# t6=row+1
	addi	$t5,$s5,1			# t5=col+1
	blt		$t6,$t5,cw_mr_t1	# (row+1) < (col+1)
	move 	$t4,$t5				# maxR = (col+1)
	j		cw_mr_e1			# escape

cw_mr_t1:
	move	$t4,$t6				# maxL = (row+1)

cw_mr_e1:

	# count left 
	li		$t0,1				# i=1

cw_forl2:
	bge		$t0,$t3,cw_forl2_e	# check escape condition
	mul		$t5,$t0,8			# 8*i
	add		$t5,$a0,$t5			# addr+8*i
	lb		$t9,($t5)			# char at	
	bne		$s7,$t9,cw_forl2_e	# break if *(addr+8*i)!=*addr
	addi	$t1,$t1,1			# left++
	addi	$t0,$t0,1			# i++
	j		cw_forl2			# loop back

cw_forl2_e:

	# count right 
	li		$t0,1				# i=1

cw_forr2:
	bge		$t0,$t4,cw_forr2_e	# check escape condition
	mul		$t5,$t0,8			# 8*i
	sub		$t5,$a0,$t5			# addr-8*i
	lb		$t9,($t5)			# char at	
	bne		$s7,$t9,cw_forr2_e	# break if *(addr-8*i)!=*addr
	addi	$t2,$t2,1			# right++
	addi	$t0,$t0,1			# i++
	j		cw_forr2			# loop back

cw_forr2_e:
	# sum diagonal points 1
	add		$t5,$t1,$t2			# t5=t1+t2
	bgt		$t5,3,cw_truereturn	#return true if > 3

	
	# diagonal check - up right and down left
	
	li		$t1,0				# left = 0
	li		$t2,0				# right = 0;
	
	# maxL = (row+1) < (7-col) ? (row+1) : (7-col);
	li		$t9,7				# t9=7
	addi	$t6,$s6,1			# t6=row+1
	sub		$t5,$t9,$s5			# t5=7-col
	blt		$t6,$t5,cw_ml_t2	# (row+1) < (7-col)
	move 	$t3,$t5				# maxL = 7-col
	j		cw_ml_e2			# escape

cw_ml_t2:
	move	$t3,$t6				# maxL = row+1

cw_ml_e2:

	# maxR = (7-row) < (col+1) ? (7-row) : (col+1);
	sub		$t6,$t9,$s6			# t6=7-row
	addi	$t5,$s5,1			# t5=col+1
	blt		$t6,$t5,cw_mr_t2	# (7-row) < (col+1)
	move 	$t4,$t5				# maxR = (col+1)
	j		cw_mr_e2			# escape

cw_mr_t2:
	move	$t4,$t6				# maxL = (7-row)

cw_mr_e2:

	# count left 
	li		$t0,1				# i=1
cw_forl3:
	bge		$t0,$t3,cw_forl3_e	# check escape condition
	mul		$t5,$t0,6			# 6*i
	sub		$t5,$a0,$t5			# addr-6*i
	lb		$t9,($t5)			# char at	
	bne		$s7,$t9,cw_forl3_e	# break if *(addr-6*i)!=*addr
	addi	$t1,$t1,1			# left++
	addi	$t0,$t0,1			# i++
	j		cw_forl3			# loop back
cw_forl3_e:

	# count right 
	li		$t0,1				# i=1
cw_forr3:
	bge		$t0,$t4,cw_forr3_e	# check escape condition
	mul		$t5,$t0,6			# 6*i
	add		$t5,$a0,$t5			# addr+6*i
	lb		$t9,($t5)			# char at	
	bne		$s7,$t9,cw_forr3_e	# break if *(addr+6*i)!=*addr
	addi	$t2,$t2,1			# right++
	addi	$t0,$t0,1			# i++
	j		cw_forr3			# loop back
cw_forr3_e:
	# sum diagonal points 2
	add		$t5,$t1,$t2			# t5=t1+t2
	bgt		$t5,3,cw_truereturn	#return true if > 3
	
	# return false
	li		$v0,0				# return false
	jr		$ra					# return
cw_truereturn:
	li		$v0,1				# return true
	jr		$ra					# return
	


print_board:
	# prints the board like so:
	# 1 2 3 4 5 6 7
    #--------------
	# . . . . . . .
	# . . . . . . .
	# . . . . . . . 
	# . . . . . . .
	# . . . . . . .
	# -------------

    	#print the numbers
    li      $v0, 4              # load string for printing
    la      $a0, numbers        # print numbers
    syscall

	#print top border
	la 		$a0,border			# load address of "-------"
	li		$v0,4				# print string
	syscall						# print
	
	# loop prep
	li		$t0,0 				# prep t0 as loop counter
	move 	$t1,$s0				# prep address for printing
	addi	$t1,$t1,48			# go to the end of the board



printloop:

    #print dots
	li		$v0,11				# print char
	lb		$a0,0($t1)			# load character for printing
	syscall						
    lb      $a0, space          # print a space
    syscall
	lb		$a0,-1($t1)			# load character for printing
	syscall						
    lb      $a0, space          # print a space
    syscall
	lb		$a0,-2($t1)			# load character for printing
	syscall						
    lb      $a0, space          # print a space
    syscall
	lb		$a0,-3($t1)			# load character for printing
	syscall						
    lb      $a0, space          # print a space
    syscall
	lb		$a0,-4($t1)			# load character for printing
	syscall						
    lb      $a0, space          # print a space
    syscall
	lb		$a0,-5($t1)			# load character for printing
	syscall						
	lb      $a0, space          # print a space
    syscall
    lb		$a0,-6($t1)			# load character for printing
	syscall						
	li		$a0,'\n'			# load a newline for printing
	syscall						
	syscall						# print it again
	addi 	$t0,$t0,1			# t0++
	sub		$t1,$t1,7			# adjust t1 to the next row
	blt		$t0,7,printloop		# check loop condition

	#print bottom border
	la 		$a0,border			# load address of "-------"
	li		$v0,4				# print string
	syscall						# print
	jr 		$ra					# return
	
		
		
get_random:
	# v0: result

	# m_z math
	move	$t0,$s1				# move m_z to a temp for later
	and 	$s1,$t0,65535 		# (m_z & 65535)
	li 		$t1,36969 			# load for multiplication
	multu 	$s1,$t1 			# m_z * 36969 (multiply unsigned)
	mflo 	$s1 				# retrieve result
	srl 	$t0,$t0,16	 		# (m_z >> 16)
	addu 	$s1,$s1,$t0 		# m_z = (&) + (>>)
	
	# m_w math
	move	$t0,$s2				# move m_w to a temp for later
	and 	$s2,$t0,65535 		# (m_w & 65535)
	li 		$t1,18000 			# load for multiplication
	multu 	$s2,$t1 			# m_w * 36969 (multiply unsigned)
	mflo 	$s2 				# retrieve result
	srl 	$t0,$t0,16	 		# (m_w >> 16)
	addu 	$s2,$s2,$t0 		# m_w = (&) + (>>)
	
	# calc return
	sll 	$s1,$s1,16			# m_z << 16
	addu 	$v0,$s1,$s2 		#  + m_w
	li 		$t0,7				# load for division
	divu 	$v0,$t0				# v0 mod 7
	mfhi	$v0					# retrieve the modulus which is 0-6
	addi	$v0,$v0,1			# make modulus 1-7
	jr 		$ra					# return
	
	
	
exit:
	li 		$a0, 0 				# return 0;
	li 		$v0, 17 			# Sets $v0 to "17" to select exit
	syscall 					# Exit





.data
	board:		.asciiz ".................................................."
	border:		.asciiz "--------------\n"
    space:      .asciiz " "
    numbers:    .asciiz "1 2 3 4 5 6 7\n"
	rand1:		.asciiz "Welcome to Connect Five!\nEnter two positive numbers to initialize the random number generator.\nRandom1:"
	rand2:		.asciiz "Random2:"
	turn:		.asciiz "What column would you like to drop in? (column 0 to quit) "
	win:		.asciiz "You won!\n"
	lose:		.asciiz "You lost!\n"
	quit:		.asciiz "You have quit\n"
	invalid:	.asciiz "That's not a valid column\n"
	thank:		.asciiz "Thank you for playing!\n"
	comp:		.asciiz "Computer selected column: \n"
	m_z:		.word 1
	m_w:		.word 1

