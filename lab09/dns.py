#!/usr/bin/env python3

# Python DNS query client
#
# Example usage:
#   ./dns.py --type=A --name=www.pacific.edu --server=8.8.8.8
#   ./dns.py --type=AAAA --name=www.google.com --server=8.8.8.8

# Should provide equivalent results to:
#   dig www.pacific.edu A @8.8.8.8 +noedns
#   dig www.google.com AAAA @8.8.8.8 +noedns
#   (note that the +noedns option is used to disable the pseduo-OPT
#    header that dig adds. Our Python DNS client does not need
#    to produce that optional, more modern header)


from dns_tools import dns  # Custom module for boilerplate code

import argparse
import ctypes
import random
import socket
import struct
import sys

def main():

    # Setup configuration
    parser = argparse.ArgumentParser(description='DNS client for ECPE 170')
    parser.add_argument('--type', action='store', dest='qtype',
                        required=True, help='Query Type (A or AAAA)')
    parser.add_argument('--name', action='store', dest='qname',
                        required=True, help='Query Name')
    parser.add_argument('--server', action='store', dest='server_ip',
                        required=True, help='DNS Server IP')

    args = parser.parse_args()
    qtype = args.qtype
    qname = args.qname
    server_ip = args.server_ip
    port = 53
    server_address = (server_ip, port)

    if qtype not in ("A", "AAAA"):
        print("Error: Query Type must be 'A' (IPv4) or 'AAAA' (IPv6)")
        sys.exit()

    # Create UDP socket
    # ---------
    # STUDENT TO-DO
    # ---------

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Generate DNS request message
    # ---------
    # STUDENT TO-DO
    # ---------
    message = bytearray() #using bytearray
    message += struct.pack("!H",random.getrandbits(16))
    class headerStruct(ctypes.BigEndianStructure) : #formatting message
        _fields_ = [
            ("QR", ctypes.c_uint16,1),
            ("Opcode", ctypes.c_uint16,4),
            ("AA", ctypes.c_uint16,1),
            ("TC", ctypes.c_uint16,1),
            ("RD", ctypes.c_uint16,1),
            ("RA", ctypes.c_uint16,1),
            ("RES", ctypes.c_uint16,3),
            ("RCODE", ctypes.c_uint16,4)
        ]

    head = headerStruct()
    head.QR = 0
    head.Opcode = 0
    head.AA = 0
    head.TC = 0
    head.RD = 1
    head.RES = 0
    head.RCODE=0
    message += bytes(head)
    message += struct.pack("!H",1) #QDCount, 
    message += struct.pack("!H",0) #ANCount, 
    message += struct.pack("!H",0) #NSCount, 
    message += struct.pack("!H",0) #ARCount

    #question section
    (ww,domain,com) = qname.split(".")
    message += struct.pack("!B",len(ww))
    message += bytes(ww,'ascii')
    message += struct.pack("!B",len(domain)) 
    message += bytes(domain,'ascii')
    message += struct.pack("!B",len(com))
    message += bytes(com,'ascii')
    message += struct.pack("!B",0)
    message += struct.pack("!H",28 if qtype == "AAAA" else 1) #QType
    message += struct.pack("!H", 1) 

    # Send request message to server
    # (Tip: Use sendto() function for UDP)
    # ---------
    # STUDENT TO-DO
    # ---------
    sock.sendto(message,server_address)

    # Receive message from server
    # (Tip: use recvfrom() function for UDP)
    # ---------
    # STUDENT TO-DO
    # ---------
    (raw_bytes,recvaddr) = sock.recvfrom(10000)

    # Close socket
    # ---------
    # STUDENT TO-DO
    # ---------
    sock.close()

    # Decode DNS message and display to screen
    dns.decode_dns(raw_bytes)


if __name__ == "__main__":
    sys.exit(main())
