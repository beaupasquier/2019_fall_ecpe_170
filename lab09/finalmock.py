#!/usr/bin/env python3

import socket
import sys
import struct
import dns


ip = '10.10.4.50'
port = 3456
num = 1
seat = "A"
name = "Beau Pasquier"
intial = "BP"
server_address = (ip,port)

#create socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print("Socket successfully created")
except socket.error as msg:
	print("Error: could not create socket")
	print("Description: " + str(msg))
	sys.exit()

print("Connecting to server at " + str(ip) + " on port " + str(port))

#connect to server
try:
    s.connect(server_address)
except socket.error as msg:
    print("Error: Server could not be found")
    print("Error code: " +str(msg))
    sys.exit();


#build request
request = bytes(struct.pack("B", seat), 'ascii')
request += (struct.pack('>IB', num))
request += (len(name), 'ascii')
request += (name, 'ascii')
request += (struct.pack("H", initial),'ascii')


#Send request
try:
    bytes_sent = s.sendall(request)
except socket.error as msg:
    print("Error sending: " + str(msg))
    sys.exit()

#closing the socket
try:
    s.close()
except socket.error as msg:
    print("Error unable to close: " + str(msg))
    sys.exit()


(raw_bytes,recvaddr) = s.recvfrom(10000)
decode_dns(raw_bytes)
print("Ticket for: " + name)







