.globl main 
.text 		

main:
	li $s7, 0		# initialize i in s7

forloop:
	# s0: n1
	# s1: n2
	# s2: gcd
	bge $s7, 10, exit 	# exit condition
	
	# random number generator
	li $a0,1		# min range
	li $a1,100000 		# max range
	jal random_in_range	# call function for first val
	move $s0,$v0 		# save first value
	jal random_in_range	# call function for next val
	move $s1,$v0 		# save second value
	
	# call hcf
	move $a0,$s0 		# n1 is first val
	move $a1,$s1 		# n2 is second val
	jal hcf			# call gcd(a,b)
	move $s2,$v0 		# save GCD value

	# call print
	move $a0,$s0		# a is arg1
	move $a1,$s1 		# b is arg2
	move $a2,$s2 		# c is arg3
	jal print		# call printf(a,b,gcd(a,b))
	
	# to continue loop
	addi $s7,$s7, 1 	#i++
	j forloop 		#loop back


random_in_range:
	# a0: low
	# a1: high
	# s0: range
	# v0: rand_num
	addi $sp,$sp,-4 	#adjust stack pointer
	sw $ra, 0($sp) 		#push ra
	addi $sp,$sp,-4 	#adjust stack pointer
	sw $s0, 0($sp) 		#push s0
	
	# set values
	subu $s0,$a1,$a0 	#range = high-low
	addiu $s0,$s0,1 	#range++
	jal get_random 		#call random number
	
	# calc return
	divu $v0,$s0 		# rand_num/range
	mfhi $v0 		# get remainder
	add $v0,$v0,$a0		# add mod + low
	
	lw $s0,0($sp)		# pop s0
	addi $sp,$sp,4 		# adjust stack pointer
	lw $ra,0($sp) 		# pop ra
	addi $sp,$sp,4		# adjust stack pointer
	jr $ra			# return

get_random:
	#Math for M_z
	lw $t0, m_z		# load m_z
	li $t1, 36969		# load t1 for multiplication
	andi $t2, $t0, 65535	# m_z & 65535
	mul $t2, $t1, $t2	# 36969 * m_z & 65535
	srl $t1, $t0, 16	# m_z >> 16
	addu $t0, $t2, $t1	# 36969 * m_z & 65535 + m_z >> 16
	sw $t0, m_z		# store m_z

	#math for m_w
	lw $t1, m_w		# load m_w
	li $t2, 18000		# load t2 for multiplication
	andi $t3, $t1, 65535	# m_w & 65535
	mul $t3, $t2, $t3	# 18000 * m_w & 65535
	srl $t2, $t1, 16	# m_w >> 16
	addu $t1, $t3, $t2	# 18000 * m_w & 65535 + m_w >> 16
	sw $t1, m_w		# store m_w

	sll $t0, $t0, 16	# m_z << 16
	addu $v0, $t0, $t1	#m_z << 16 + m_w 
	jr $ra

hcf:
	#a0: n1
	#a1: n2
	
	#check modulo != 0
	divu $a0,$a1 		# n1/n2
	mfhi $t0 		# grab remainder
	beq $t0,0,hcfreturn	
	
	move $a0,$a1		# set n2 as arg1 of hcf
	move $a1,$t0		# n1%n2 as arg2 of hcf
	j hcf 			#recurse through until = 0

hcfreturn:
	move $v0,$a1 		#return n1
	jr $ra

print: 				# Specific to this program
	move $t0,$a0 		# move a0 to temp to print

	#print "GCD of "
	la $a0,str0 		# load address
	li $v0,4 		# print string
	syscall
	
	#print n2
	move $a0,$t0		# prep n2 as argument
	li $v0,1 		# print int
	syscall
	
	#print "and"
	la $a0,str1		# load character
	li $v0,4 		# print string
	syscall
	
	#print n1
	move $a0,$a1 		# prep argument
	li $v0,1 		# print int
	syscall
	
	#print " is "
	la $a0,str2		# load address
	li $v0,4 		# print string
	syscall
	
	#print gcd
	move $a0,$a2 		# prep argument
	li $v0,1 		# print int
	syscall
	
	#print \n
	li $a0,'\n' 		# load character
	li $v0,11 		# print char
	syscall
	
	jr $ra
exit:
	li $a0, 0 		# return 0;
	li $v0, 17		# Sets $v0 to "17" to select exit2 syscall
	syscall 		# Exit
	
	
.data
m_w:	.word 50000
m_z:	.word 60000
str0:	.asciiz "GCD of "
str1:	.asciiz " and "
str2:	.asciiz " is "

