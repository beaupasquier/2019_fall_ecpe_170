.globl main 
.text 		
	

main:
	# s0: array
	# s1: arraySize
	# s2: result
	
	
	# load addresses 
	la $s0,array 		    #array[] = {2,3,5,7,11}
	lw $s1,arraySize 	    #arraySize = 5
    li $s7, 0              	# initialize i in s7
	
forloop:

    bge $s7, $s1 exit
    
    ## printf for array
	la $a0,msg0             #load string for printing
	li $v0,4	            # 4=print_string
	syscall

	move $a0,$s7	        # load int for printing (i)
	li $v0,1	            # 1=print_int
	syscall

	la $a0,msg1             #load string for printing
	li $v0,4	            # 4=print_string
	syscall


	lw $a0, 0($s0)         # load int for printing (result)
	li $v0,1	            # 1=print_int
	syscall

	li $a0,'\n'         	# load a newline for printing
	li $v0,11	            # 11=print_char
	syscall

    addi $s7,$s7, 1 
    j forloop

    j arraySum


	la $a0,msg2             #load string for printing
	li $v0,4	            # 4=print_string
	syscall



arraySum:

	# push registers, copy arguments
	addi 	$sp,$sp,-4 			# adjust stack pointer
	sw 		$ra, 0($sp) 		# push ra
	addi 	$sp,$sp,-4 			# adjust stack pointer
	sw 		$s0, 0($sp) 		# push s0
	addi 	$sp,$sp,-4 			# adjust stack pointer
	sw 		$s1, 0($sp) 		# push s1

	move $s0,$a0 				#copy *array to s0
	move $s1,$a1 				#copy arraySize to s1
    move $s2,$a2                #copy result to s2

	beq $s1, 0, return0         #return zero if arraySize = 0

	add $s0, $s0, $s2	        #add result to t0
	sw $s0, $s2                 #save t0 to A[i]
	addi $s0, $s0, 4	        #add 4 to get the next address

	j arraySum	                # recurse

	



return0:
    move $v0, $s2

    # pop registers, return
	lw		$s2,0($sp) 			# pop s2
	addi 	$sp,$sp,4 			# adjust stack pointer
	lw		$s1,0($sp) 			# pop s1
	addi 	$sp,$sp,4 			# adjust stack pointer
	lw		$s0,0($sp) 			# pop s0
	addi 	$sp,$sp,4 			# adjust stack pointer
	lw		$ra,0($sp) 			# pop ra
	addi 	$sp,$sp,4 			# adjust stack pointer
    

	jr $ra	#return







exit:
	li $v0, 17		        # Sets $v0 to "17" to select exit2 syscall
	syscall 		        # Exit

.data
	array: .word 2, 3, 5, 7, 11  #{2,3,5,7,11}
	arraySize: .word 5
	result: .space 4
    msg0: .asciiz "Array["
    msg1: .asciiz "]= "
    msg2: .asciiz "Sum of array is "

