#!/usr/bin/env python3

import socket
import sys
import struct


ip = '10.10.4.50'
port = 3456
version = 1
numA = 1
numB = -2
numC = -4
name = "Beau Pasquier"
server_address = (ip,port)

# Create socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error as msg:
	print("Error: could not create socket")
	print("Description: " + str(msg))
	sys.exit()


print("Connecting to server at " + ip + " on port " + str(port))

#connect to server
try:
    s.connect(server_address)
    print("Connected to server")
except socket.error as msg:
    print("Error: Server could not be found")
    print("Error code: " +str(msg))
    sys.exit();

#build request
request = bytes(struct.pack("B", version))		#adding version number
request += (struct.pack(">l", numA)) 			#adding numA
request += (struct.pack(">l", numB))			#adding numB
request += (struct.pack(">l", numC))			#numC
request += (struct.pack(">l",len(name)))
request += (bytes(name, 'ascii'))


#Send request
try:
    s.sendto(request, server_address)
    print("Sent request to " + str(server_address))
except socket.error as msg:
    print("Error sending: " + str(msg))
    sys.exit()

#receive reply
try:
    data = s.recvfrom(10000)
    print(data)
except:
    print("Could not find data")

#close socket
try:
    s.close()
    print("Closed socket")
except socket.error as msg:
    print("Error unable to close: " + str(msg))
    sys.exit()



print("Finished program")

sys.exit()


















