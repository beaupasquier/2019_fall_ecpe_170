#include "config.h"
#include "vec.h"
#include "combine.h"

#include <stdio.h>

// ORIGINAL function.
// This combiner function uses the greater amount
// of abstraction to operate, but has the slowest
// performance.
void combine1(vec_ptr v, data_t *dest)
{
  printf("Running combine1() - No code-level optimizations\n");

  long int i;

  *dest = IDENT;

  for(i=0; i < vec_length(v); i++)
    {
      data_t val;
      get_vec_element(v, i, &val);
      *dest = *dest OP val;
    }
}


// CODE MOTION OPTIMIZATION:

// Move the call to vec_length() out of the loop
// because we (the programmer) know that the vector length will
// not change in the middle of the combine() function. 
//  The compiler, though, doesn't know that!
void combine2(vec_ptr v, data_t *dest)
{
  printf("Running combine2()\n");
  printf("Added optimization: Code motion\n");

  // XXX - STUDENT CODE GOES HERE - XXX

  long int i;

int len = vec_length(v); // doesn't have to call each time throught the loop
  for(i=0; i < len; i++)
    {
      data_t val;
      get_vec_element(v, i, &val);
      *dest = *dest OP val;
    }

}


// REDUCING PROCEDURE CALLS OPTIMIZATION:

// This optimization eliminates the function call to
// get_vec_element() and accesses the data directly,
// trading off higher performance versus some loss
// of program modularity.
void combine3(vec_ptr v, data_t *dest)
{
  printf("Running combine3()\n");
  printf("Added optimization: Reducing procedure calls\n"); 

  // XXX - STUDENT CODE GOES HERE - XXX

  long int i;

  *dest = IDENT;

  int len = vec_length(v);
  for(i=0; i < len; i++)
    {
      *dest = *dest OP v->data[i]; // grabbing the data directly
    }

}


// ELIMINATING UNNEEDED MEMORY ACCESSES OPTIMIZATION:

// This optimization eliminates the trip to memory
// to store the result of each operation (and retrieve it
// the next time). Instead, it is saved in a local variable 
// (i.e. a register in the processor)
// and only written to memory at the very end.
void combine4(vec_ptr v, data_t *dest)
{
  printf("Running combine4()\n");
  printf("Added optimization: Eliminating unneeded memory accesses\n");

  long int i;

   *dest = IDENT;
  data_t temp = 0; // create temp to point data at after
  int len = vec_length(v);
  for(i=0; i < len; i++)
    {
      temp = temp OP v->data[i];
    }
	*dest = temp; //point data to temp

}


// LOOP UNROLLING x2
// (i.e. process TWO vector elements per loop iteration)
void combine5x2(vec_ptr v, data_t *dest)
{
  printf("Running combine5x2()\n");
  printf("Added optimization: Loop unrolling x2\n");

  // XXX - STUDENT CODE GOES HERE - XXX

  long int i;

  *dest = IDENT;
  data_t temp = 0;
  int len = vec_length(v);
  for(i=0; i < len-1; i += 2)
    {
      temp = temp OP v->data[i]; //getting even indexes
      temp = temp OP v->data[i + 1]; //odd indexes
    }
    if(len % 2 != 0) 
    {
    	temp = temp OP v->data[len-1]; //don't forget last value if len odd
    }
	*dest = temp;

}

// LOOP UNROLLING x3
// (i.e. process THREE vector elements per loop iteration)
void combine5x3(vec_ptr v, data_t *dest)
{
  printf("Running combine5x3()\n");
  printf("Added optimization: Loop unrolling x3\n");

  // XXX - STUDENT CODE GOES HERE - XXX

  long int i;

  *dest = IDENT;
  data_t temp = 0;
  int len = vec_length(v);
  for(i=0; i < len-2; i++) //same as other but with %3 instead 2
    {
      temp = temp OP v->data[i];
      temp = temp OP v->data[i++];
      temp = temp OP v->data[i++];
    }
    if(len % 3 == 1) { //including last values
    	temp = temp OP v->data[len-1];
    }
    if(len % 3 == 2) { //if there are 2 extra values at the end after loops ends
    	temp = temp OP v->data[len-1];
    	temp = temp OP v->data[len-2];
    }
	*dest = temp;

}


// LOOP UNROLLING x2 + 2-way parallelism
void combine6(vec_ptr v, data_t *dest)
{
  printf("Running combine6()\n");
  printf("Added optimization: Loop unrolling x2, Parallelism x2\n");

  // XXX - STUDENT CODE GOES HERE - XXX

  long int i;

  *dest = IDENT;
  data_t temp = 0;
  data_t temp2 = 0; //create a temp 2 for parellelism
  int len = vec_length(v);
  for(i=0; i < len-1; i += 2)
    {
      temp = temp OP v->data[i]; //same as unrolling loop but with 2nd temp
      temp2 = temp2 OP v->data[i + 1];
    }
    if(len % 2 != 0) {
    	temp = temp OP v->data[len-1];
    }
	*dest = temp + temp2; //combine temps

}
