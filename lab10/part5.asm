# Declare main as a global function
.globl main

# All program code is placed after the
# .text assembler directive
.text 		

# The label 'main' represents the starting point
main:
	li $s0, 0x0 #RESULT
	la $a0, inString #InString
	lw $a1, strLen
	li	$v0,8		# read_string syscall code = 8
	syscall	
	li $t2, 0		#set up i
searchLoop:
	lb $t0, ($a0)			#char at i
	beq $t0, 10, notFound	#if you reach the end of the buffer and didn't find an "e"	
	beq $t0, 101, found		#if an e is found, exit the loop
	addi $a0, $a0, 1		#add 1 (length of char) to a0
	j searchLoop			#loop back
found:
	move $s0, $a0	#store result address
	la $a0, str1	#load str1 to print
	li $v0, 4		#4 in v0 is print_string
	syscall
	move $a0, $s0	#load address at s0
	li $v0, 1		# print integer
	syscall
	la $a0, str2	#load str2 to print
	li $v0, 4		#4 in v0 is print_string
	syscall
	lb $a0, ($s0)	#load address at s0
	li $v0, 11		# print char
	syscall
	sw $s0, result	
	j exit			#jump to exit
notFound:
	la $a0, str3	#prep to pritn str3
	li $v0, 4		# print_string
	syscall
exit:
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

# All memory structures are placed after the
# .data assembler directive
.data


# The .word assembler directive reserves space
# in memory for a single 4-byte word (or multiple 4-byte words)
# and assigns that memory location an initial value
# (or a comma separated list of initial values)	#For example:
# value:	.word 12

result:		.space 4
inString:	.space 256
strLen:		.word 256
str1:		.asciiz "First match at address "
str2:		.asciiz "\nThe matching character is "
str3:		.asciiz "No match found"

