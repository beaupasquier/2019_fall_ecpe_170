# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		
	

# The label 'main' represents the starting point
main:
	li $s1, 15		# A=15 		use li to load
	li $s2, 10		# B=10		immediately into
	li $s3, 7		# C=7		that register
	li $s4, 2		# D=2
	li $s5, 18		# E=18
	li $s6, -3		# F=-3
	add $t0, $s1, $s2	# A+B = t0
	sub $t1, $s3, $s4	# C-D = t1
	add $t2, $s5, $s6	# E+F = t2
	sub $t3, $s1, $s3	# A-C = t3
	add $t4, $t0, $t1	# (A+B) + (C-D) = t4
	sub $t5, $t2, $t3	# (E+F) - (A-C) = t5
	add $t6, $t4, $t5	#final sum = t6
	sw $t6, Z			#save the sum to memory Z
	
	
	
	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
Z:	.word 0	

