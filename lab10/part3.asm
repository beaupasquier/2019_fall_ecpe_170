# Declare main as a global function
.globl main
# All program code is placed after the
# .text assembler directive
.text

	
# The label 'main' represents the starting point
main:
	lw $s0, Z	# load z
	li $s1, 0	# create i in S1
loop1:
	bgt $s1, 20, loop2 #check to break to loop 2
	addi $s0, $s0, 1	#z++
	addi $s1, $s1, 2 	#i+=2
	j loop1	#loop back
loop2:
	addi $s0, $s0, 1	# z++
	bge $s0, 100, loop3	# escape if satisfied to loop 3
	j loop2		#loop back
loop3:
	ble $s1, 0, exit	#check exit condition
	addi $s0, $s0, -1 #z-- (subi doesn't exist)
	addi $s1, $s1, -1 #i--
	j loop3
exit:
	sw $s1, I	#store I
	sw $s0, Z	#store Z
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit


# All memory structures are placed after the
# .data assembler directive
	.data

# The .word assembler directive reserves space
# in memory for a single 4-byte word (or multiple 4-byte words)
# and assigns that memory location an initial value
# (or a comma separated list of initial values)
#For example:
#value:	.word 12
I:	.word 0
Z:	.word 2

