# Declare main as a global function
.globl main 

# All program code is placed after the
# .text assembler directive
.text 		

# The label 'main' represents the starting point
main:
	la $s0, A	# load A's base address
	la $s1, B	# load B's base address
	lw $s2, C	# load C
	li $s3, 0	# create i in S3
loop1:
	bge $s3, 5, stop1	#exit condition (i >= 5)
	lw $t0, ($s1)	#load B[i] to t0
	add $t0, $t0, $s2	# add C to t0
	sw $t0, ($s0)	#save t0 to A[i]
	addi $s0, $s0, 4	#add 4 to get the next address
	addi $s1, $s1, 4	#add 4 to get the next address
	addi $s3, $s3, 1 	#i++
	j loop1	# loop back
stop1:
	sub $s3, $s3, 1	#i--
	sub $s0, $s0, 4	#sub 4 to get the proper address
loop2:
	blt $s3, 0, exit	# exit condition (i < 0)
	lw $t0, ($s0)	#load A[i] to t0
	mul $t0, $t0, 2	# double A[i]
	sw $t0, ($s0)	#save temp to A[i]
	sub $s0, $s0, 4	#sub 4 to get the next address
	sub $s3, $s3, 1	#i--
	j loop2	#loop back
exit:
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

# All memory structures are placed after the
# .data assembler directive
.data

# The .word assembler directive reserves space
# in memory for a single 4-byte word (or multiple 4-byte words)
# and assigns that memory location an initial value
# (or a comma separated list of initial values)
#For example:
#value:	.word 12

A:	.space 20	#allocate space for 5 words
B:	.word 1, 2, 3, 4, 5	#initialize A as array
C:	.word 12	#initialize C

