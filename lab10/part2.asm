# Declare main as a global function
.globl main 

# All program code is placed after the
# .text assembler directive
.text 		


# The label 'main' represents the starting point
main:
	lw $s0, A		# A in s0
	lw $s1, B		# B in s1
	lw $s2, C		# C in s2
	sgt $t1, $s0, $s1	# t1 = (A > B)
	slti $t2, $s2, 5 	# t2 = (C < 5)
	or $t0, $t1, $t2	# t0 = t1 OR t2
	bne $t0, $zero, z1	# if t0 != 0 jump to z1
	addi, $t2, $s2, 1	# store C+1 to t2
	seq $t2, $t2, 7		# t2 is ((C+1)==7) 
	and $t0, $t2, $t3	# t0 is t1 && t2
	bne $t0, $zero, z2	# if t0 jump to t2
	li $s3, 3			# Z=3
	j switch			# switch statement
	
z1:
	li $s3, 1	# Z=1
	j switch	# switch statement
z2:
	li $s3, 2	# Z=2
	j switch	# switch statement
	
switch:
	beq $s3, 1, sw1	# case 1
	beq $s3, 1, sw2	# case 2
#default case
	li $s3, 0
	j exit
sw1:
	li $s3, -1	# s3 = -1
	j exit		# jump to exit
sw2:
	sub $s3, $s3, -2	# s3 -= -2
	j exit		# jump to exit
exit:
	sw $s3, Z	# save Z
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit


# All memory structures are placed after the
# .data assembler directive
.data

# The .word assembler directive reserves space
# in memory for a single 4-byte word (or multiple 4-byte words)
# and assigns that memory location an initial value
# (or a comma separated list of initial values)
#For example:
#value:	.word 12

A:	.word 10	# A is stored
B:	.word 15	# B is stored
C:	.word 6		# C is stored
Z:	.word 0		# Z is stored

