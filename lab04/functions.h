#ifndef FUNCTIONS_H
#define FUNCTIONS_H


void display(int width, int height, char **array);

void findWord(char **array, int width, int height, char *str);

void startSearch(char **array, int width, int height, const char *str, int index, int r, int c);

int contSearch(char **array, int width, int height, const char *str, int index, int r, int c, int y, int x);



#endif
