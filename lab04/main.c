//Name: Beau Pasquier
//Email: b_pasquier@u.pacific.edu

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include"functions.h"

	int main()
	{
    		FILE *ptr_file;
		char wordInput[31];
		char **array;
		int width, height;
		

    		ptr_file =fopen("input.txt","r");
    		if (!ptr_file)
        		return 1;

		
		fscanf(ptr_file, "%d %d", &height, &width);
		printf("%d %d", height, width);
		fgetc(ptr_file);
		printf("\n\n");		
		


		array = (char **)malloc(sizeof(char *)*height);
		for (int i = 0; i < height; i++)
			array[i] = (char *)malloc(sizeof(char)*width);

		char ch;
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				ch = fgetc(ptr_file);
				if(ch == '\n')
				{
				array[i][j] = tolower(fgetc(ptr_file));
				}
				else
				array[i][j] = tolower(ch);
			}
		}

		display(width, height, array);

		printf("Enter a word:\n");
		scanf("%s", wordInput);
		printf("Finding %s...\n\n", wordInput);


		while(strcmp(wordInput, "exit") != 0) //type exit to exit
		{
			findWord(array, width, height, wordInput);
			display(width, height, array);
			printf("\nEnter a word:\n");
			scanf("%s", wordInput);
			printf("Finding %s...\n\n", wordInput);
		}
		printf("Thanks for playing\n");
		
		for(int i = 0; i < height; i++) //deallocated memory
		free(array[i]);
		free(array);


		fclose(ptr_file);
    		return 0;
	}
