#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include "functions.h"

void display(int width, int height, char **array)
{
	for(int i = 0; i < height; i++)
	{
		for(int j = 0; j < width; j++)
		{					/*traverse the array*/
  			printf("%c", array[i][j]);      /*Print as you go*/  	
		}
	printf("\n");
	}
}


void startSearch(char **array, int width, int height, const char *str, int index, int r, int c) { //search for word in all directions
	for(int y = -1; y < 2; y++) {
		for(int x = -1; x < 2; x++) {
			if(r+y < 0 || c+x < 0 || r+y >= height || c+x >= width)
				continue;
			if (contSearch(array, width, height, str, index - 1, r+y, c+x, y, x))
				array[r][c] = toupper(array[r][c]);
		}
	}
}


void findWord(char **array, int width, int height, char *str) {		//Searches array for matching words if found it searches
	int len = strlen(str);
	for(int r = 0; r < height; r++) {
		for(int c = 0; c < width; c++) {
			if (tolower(array[r][c]) == str[len-1])
				startSearch(array, width, height, str, len-1, r, c);
		}
	}
}




int contSearch(char **array, int width, int height, const char *str, int index, int r, int c, int y, int x) { //continues search once letter is found and switches to uppercase letters
	if (r < 0 || c < 0 || c >= width || r >= height)
		return 0;
	if(tolower(array[r][c]) == str[index]) {
		if (index == 0) {
			array[r][c] = toupper(array[r][c]);
			return 1;
		}
		if (contSearch(array, width, height, str, index - 1, r + y, c + x, y, x)) {
			array[r][c] = toupper(array[r][c]);
			return 1;
		}
		return 0;	
	}
	return 0;
}


